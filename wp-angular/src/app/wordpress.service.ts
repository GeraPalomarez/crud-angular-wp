import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import { map } from 'rxjs/operators';
import { Observable, of, Subject } from 'rxjs';



@Injectable ()
export class WordpressService {
  constructor(private http:Http) { }

  getProductos() {
      return this.http.get('http://localhost/L&L/wp-json/wp/v2/producto/').pipe(map(res => res.json()));
  }
  getDetailProducto(id) {
    return this.http.get('http://localhost/L&L/wp-json/wp/v2/producto/'+id).pipe(map(res => res.json()));
  }
  getPaginas() {
    return this.http.get('http://localhost/L&L/wp-json/wp/v2/pages/').pipe(map(res => res.json()));
  }
}
