import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SingleRoutingModule } from './single-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SingleRoutingModule
  ],
  declarations: []
})
export class SingleModule { }
