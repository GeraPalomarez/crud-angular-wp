import { Component, OnInit } from '@angular/core';
import { WordpressService } from '../../wordpress.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  providers: [WordpressService]
})
export class DetailsComponent implements OnInit {
  public detalles;
  public title;
  public id;

  constructor(
    private wpService: WordpressService,
    private _route : ActivatedRoute,
    private _router : Router,
    ) {
      this.detalles = "";
      this.title = "";
  }

  ngOnInit() {
    this.detalleProducto();
  }

  detalleProducto() {
    this._route.params.forEach((params: Params) => {
      this.id= params['id'];
    this.wpService.getDetailProducto(this.id).subscribe( 
      result => {
            this.title = result.title.rendered;
            this.detalles = result.acf;
            //  console.log(this.detalles);
        },
        error => {
        var errorMessage = <any>error;
        console.log(errorMessage);
        }
    );
  });
}

}
