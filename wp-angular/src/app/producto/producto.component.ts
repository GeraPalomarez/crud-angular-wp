import { Component, OnInit } from '@angular/core';
import {WordpressService} from '../wordpress.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.scss'],
  providers: [WordpressService]
})
export class ProductoComponent implements OnInit {
  public productos: any;

  constructor(
    private wpService: WordpressService,
  ) {
    console.log("Entro al componente de producto");
   }

  ngOnInit() {
    this.listaProductos();
  }
  
  listaProductos() {
    //console.log("Entro en listaProductos");
    this.wpService.getProductos().subscribe( 
      result => {
            this.productos = result;
           console.log(this.productos);
        },
        error => {
        var errorMessage = <any>error;
        console.log(errorMessage);
        }
    );
  }
}
