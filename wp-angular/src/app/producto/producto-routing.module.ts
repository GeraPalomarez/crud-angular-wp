import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailsComponent } from './details/details.component';
import { ProductoComponent } from './producto.component';

const routes: Routes = [ 
  {
    path: '',
    component:ProductoComponent
  },
  {
    path: ':id',
    component:DetailsComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductoRoutingModule { }
