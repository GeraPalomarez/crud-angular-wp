import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//import {DetailsComponent} from './details/details.component';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { HttpModule } from '@angular/http';
//import { ProductoComponent } from './producto/producto.component';


@NgModule({
  declarations: [
    AppComponent,
    //DetailsComponent,
    //ProductoComponent
  ],
  imports: [
    BrowserModule,
    CoreModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
