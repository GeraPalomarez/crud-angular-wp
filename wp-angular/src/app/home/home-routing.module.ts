import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainCarouselComponent } from './main-carousel/main-carousel.component';
import { HomeComponent } from './home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      { path: '', component: MainCarouselComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
