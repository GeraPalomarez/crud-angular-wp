import { ModuleWithProviders } from '@angular/core';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, Params } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: '../home/home.module#HomeModule'
  },
  {
    path: 'producto',
    loadChildren: '../producto/producto.module#ProductoModule'
  },
  {
    path: ':page-name',
    loadChildren: '../pages/pages.module#PagesModule'
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }

