(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["producto-producto-module"],{

/***/ "./src/app/producto/details/details.component.html":
/*!*********************************************************!*\
  !*** ./src/app/producto/details/details.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-12\">\n      <div class=\"card mt-5\">\n        <strong><h1 class=\"card-header text-center\">{{title}}</h1></strong>\n        <img class=\"card-img-top\" src=\"{{detalles.imagen}}\" alt=\"Card image cap\">\n        <div class=\"card-body\">\n          <h5 class=\"card-title\">{{ detalles.Titulo }}</h5>\n          <p class=\"card-text\">{{ detalles.Descripcion }}.  </p>\n          <div class=\"row text-right\">\n            <div class=\"col-sm\">\n            <h2><span class=\"text-success\">Precio de venta: </span> ${{ detalles.precio}}</h2> \n            </div>\n          </div>\n          <a href=\"{{ detalles.url}}\" target=\"_blank\" class=\"btn btn-success\">Comprar</a>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n "

/***/ }),

/***/ "./src/app/producto/details/details.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/producto/details/details.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/producto/details/details.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/producto/details/details.component.ts ***!
  \*******************************************************/
/*! exports provided: DetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailsComponent", function() { return DetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _wordpress_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../wordpress.service */ "./src/app/wordpress.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DetailsComponent = /** @class */ (function () {
    function DetailsComponent(wpService, _route, _router) {
        this.wpService = wpService;
        this._route = _route;
        this._router = _router;
        this.detalles = "";
        this.title = "";
    }
    DetailsComponent.prototype.ngOnInit = function () {
        this.detalleProducto();
    };
    DetailsComponent.prototype.detalleProducto = function () {
        var _this = this;
        this._route.params.forEach(function (params) {
            _this.id = params['id'];
            _this.wpService.getDetailProducto(_this.id).subscribe(function (result) {
                _this.title = result.title.rendered;
                _this.detalles = result.acf;
                //  console.log(this.detalles);
            }, function (error) {
                var errorMessage = error;
                console.log(errorMessage);
            });
        });
    };
    DetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-details',
            template: __webpack_require__(/*! ./details.component.html */ "./src/app/producto/details/details.component.html"),
            styles: [__webpack_require__(/*! ./details.component.scss */ "./src/app/producto/details/details.component.scss")],
            providers: [_wordpress_service__WEBPACK_IMPORTED_MODULE_1__["WordpressService"]]
        }),
        __metadata("design:paramtypes", [_wordpress_service__WEBPACK_IMPORTED_MODULE_1__["WordpressService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], DetailsComponent);
    return DetailsComponent;
}());



/***/ }),

/***/ "./src/app/producto/producto-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/producto/producto-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: ProductoRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductoRoutingModule", function() { return ProductoRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _details_details_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./details/details.component */ "./src/app/producto/details/details.component.ts");
/* harmony import */ var _producto_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./producto.component */ "./src/app/producto/producto.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '',
        component: _producto_component__WEBPACK_IMPORTED_MODULE_3__["ProductoComponent"]
    },
    {
        path: ':id',
        component: _details_details_component__WEBPACK_IMPORTED_MODULE_2__["DetailsComponent"]
    },
];
var ProductoRoutingModule = /** @class */ (function () {
    function ProductoRoutingModule() {
    }
    ProductoRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ProductoRoutingModule);
    return ProductoRoutingModule;
}());



/***/ }),

/***/ "./src/app/producto/producto.component.html":
/*!**************************************************!*\
  !*** ./src/app/producto/producto.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container mt-5\" >\n  <div class=\"row\">\n    <div class=\"col-sm-12 col-lg-4\" *ngFor=\"let producto of productos\">\n      <div class=\"card-deck\">\n        <div class=\"card\">\n          <img class=\"card-img-top\" src=\"{{producto.acf.imagen}}\" alt=\"Card image cap\">\n          <div class=\"card-body\">\n            <h5 class=\"card-title\">{{producto.title.rendered}}</h5>\n            <p class=\"card-text\">Some quick example text to build on the card title and make up the bulk of the card's\n              content.</p>\n            <a routerLink=\"{{producto.id}}\" class=\"btn btn-primary\">Ver más información</a>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/producto/producto.component.scss":
/*!**************************************************!*\
  !*** ./src/app/producto/producto.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/producto/producto.component.ts":
/*!************************************************!*\
  !*** ./src/app/producto/producto.component.ts ***!
  \************************************************/
/*! exports provided: ProductoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductoComponent", function() { return ProductoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _wordpress_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../wordpress.service */ "./src/app/wordpress.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductoComponent = /** @class */ (function () {
    function ProductoComponent(wpService) {
        this.wpService = wpService;
        console.log("Entro al componente de producto");
    }
    ProductoComponent.prototype.ngOnInit = function () {
        this.listaProductos();
    };
    ProductoComponent.prototype.listaProductos = function () {
        var _this = this;
        //console.log("Entro en listaProductos");
        this.wpService.getProductos().subscribe(function (result) {
            _this.productos = result;
            console.log(_this.productos);
        }, function (error) {
            var errorMessage = error;
            console.log(errorMessage);
        });
    };
    ProductoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-producto',
            template: __webpack_require__(/*! ./producto.component.html */ "./src/app/producto/producto.component.html"),
            styles: [__webpack_require__(/*! ./producto.component.scss */ "./src/app/producto/producto.component.scss")],
            providers: [_wordpress_service__WEBPACK_IMPORTED_MODULE_1__["WordpressService"]]
        }),
        __metadata("design:paramtypes", [_wordpress_service__WEBPACK_IMPORTED_MODULE_1__["WordpressService"]])
    ], ProductoComponent);
    return ProductoComponent;
}());



/***/ }),

/***/ "./src/app/producto/producto.module.ts":
/*!*********************************************!*\
  !*** ./src/app/producto/producto.module.ts ***!
  \*********************************************/
/*! exports provided: ProductoModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductoModule", function() { return ProductoModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _producto_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./producto-routing.module */ "./src/app/producto/producto-routing.module.ts");
/* harmony import */ var _producto_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./producto.component */ "./src/app/producto/producto.component.ts");
/* harmony import */ var _details_details_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./details/details.component */ "./src/app/producto/details/details.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ProductoModule = /** @class */ (function () {
    function ProductoModule() {
    }
    ProductoModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _producto_routing_module__WEBPACK_IMPORTED_MODULE_2__["ProductoRoutingModule"]
            ],
            declarations: [_producto_component__WEBPACK_IMPORTED_MODULE_3__["ProductoComponent"], _details_details_component__WEBPACK_IMPORTED_MODULE_4__["DetailsComponent"]]
        })
    ], ProductoModule);
    return ProductoModule;
}());



/***/ })

}]);
//# sourceMappingURL=producto-producto-module.js.map