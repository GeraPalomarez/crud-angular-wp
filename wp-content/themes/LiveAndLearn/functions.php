<?php

require_once('functions/robots.php');
require_once('functions/config.php');

function wpb_adding_scripts()
{
    wp_deregister_script('jquery');
    wp_deregister_script('wp-api');

    if (WP_DEBUG === true) {
      wp_register_script('runtime', get_template_directory_uri() . '/dist/' . ANGULAR_JS_RUNTIME, array(), false, true);
      wp_register_script('polyfills', get_template_directory_uri() . '/dist/' . ANGULAR_JS_POLYFILLS, array(), false, true);
      wp_register_script('styles', get_template_directory_uri() . '/dist/' . ANGULAR_JS_STYLES, array(), false, true);  
      wp_register_script('vendor', get_template_directory_uri() . '/dist/' . ANGULAR_JS_VENDOR, array(), false, true);  
      wp_register_script('main', get_template_directory_uri() . '/dist/' . ANGULAR_JS_MAIN, array(), false, true);  
      wp_register_script('home', get_template_directory_uri() . "/dist/home-home-module.js", array(), false, true);  
      wp_register_script('page', get_template_directory_uri() . "/dist/pages-pages-module.js", array(), false, true);  
      wp_register_script('producto', get_template_directory_uri() . "/dist/producto-producto-module.js", array(), false, true);  
     
      wp_enqueue_script('home');
      wp_enqueue_script('page');
      wp_enqueue_script('producto');
      
    } else {
      wp_register_script('runtime', get_template_directory_uri() . '/dist/' . PROD_ANGULAR_JS_RUNTIME, array(), false, true);
      wp_register_script('polyfills', get_template_directory_uri() . '/dist/' .PROD_ANGULAR_JS_POLYFILLS, array(), false, true);
      wp_register_script('styles', get_template_directory_uri() . '/dist/' . PROD_ANGULAR_CSS_STYLES, array(), false, true);  
      wp_register_script('main', get_template_directory_uri() . '/dist/' . PROD_ANGULAR_JS_MAIN, array(), false, true);  
    }

   
    //Push WP settings to the app
    $appSettings = array(
        'menu' => array("home","blog","about"),
        'categories' => array("news","sports","technology"),
        'routes' => array(
            array(
                "name" => "Posts",
                "path" => "/posts"
            ),
        ),
        'config' => array(
            'thumbnail_size' => "150",
            'featured_size' => "720",
            'posts_per_page' => "10",
            'theme_class' => "dark"
        ),
    );

    wp_localize_script( 'inline', 'settings', $appSettings );

    if (WP_DEBUG === true) {
      wp_enqueue_script('runtime');
      wp_enqueue_script('polyfills');
      wp_enqueue_script('styles');
      wp_enqueue_script('vendor');
      wp_enqueue_script('main');

    }else {
      wp_enqueue_script('runtime');
      wp_enqueue_script('polyfills');
      wp_enqueue_script('main');
    }
    wp_enqueue_script('runtime');
    wp_enqueue_script('polyfills');
    wp_enqueue_script('styles');
    wp_enqueue_script('vendor');
    wp_enqueue_script('main');
 



    
    //wp_localize_script( $handle, $name, $data ); 
}
add_action('wp_enqueue_scripts', 'wpb_adding_scripts');

function wpb_adding_styles()
{
  if (WP_DEBUG === true) { // DESARROLLO
    
  } else { // PRODUCCION
    wp_enqueue_style('style', get_template_directory_uri(). '/dist/' . PROD_ANGULAR_CSS_STYLES);
  }
  
}
add_action('wp_enqueue_scripts', 'wpb_adding_styles');


/*
function wpb_adding_scripts_single (){
  wp_deregister_script('jquery');
  wp_deregister_script('wp-api');

  if (is_singular('post')) {
    wp_register_script('home-home', get_template_directory_uri() . '/dist/home-home-module.js,', array(), false, true);

    wp_enqueue_script('home-home');
  }

}

add_action('wp_enqueue_scripts', 'wpb_adding_scripts_single');

function wpb_adding_scripts_pages (){

  if (is_page()) {
    if (WP_DEBUG === true) {
      wp_register_script('pages-pages', get_template_directory_uri() . '/dist/pages-pages-module.js', array(), false, true);
    } else {
      wp_register_script('pages-pages', get_template_directory_uri() . '/dist/0.ea3a9b3e67c2989d520b.js', array(), false, true);
    }


    wp_enqueue_script('pages-pages');
  }

}

function wpb_adding_scripts_producto (){

  if (is_post_type_archive( 'producto' )) {
    if (WP_DEBUG === true) {
      wp_register_script('producto-producto-module', get_template_directory_uri() . '/dist/producto-producto-module.js', array(), false, true);
    } else {
      wp_register_script('producto-producto-module', get_template_directory_uri() . '/dist/0.ea3a9b3e67c2989d520b.js', array(), false, true);
    }


    wp_enqueue_script('producto-producto-module');
  }

}

add_action('wp_enqueue_scripts', 'wpb_adding_scripts_producto');
*/

function create_posttype() {
  register_post_type( 'producto',
    array(
      'labels' => array(
        'name' => __( 'Producto' ),
        'singular_name' => __( 'Producto' )
      ),
      'public' => true,
      'has_archive' => true,
      'show_in_rest' => true,
      'rewrite' => array('slug' => 'producto'),
    )
  );
}
add_action( 'init', 'create_posttype' );